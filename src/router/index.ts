import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue';
import Favourite from '@/views/Favourites.vue';
import MovieDetails from '@/views/MovieDetails.vue';
import {Movie} from '@/models/models';
import store from '@/store';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/favourite',
    name: 'favourite',
    component: Favourite,
  },
  {
    path: '/movie/:id',
    name: 'movie',
    component: MovieDetails,
    beforeEnter: async (to: any, from: any, next: (redirect?: string) => void) => {
      const id = to.params.id;
      const popularMovies: Movie[] = store.getters.getPopularMovies;
      const movie = popularMovies.find((m) => `${m.id}` === id);
      if (movie) {
        store.dispatch('setMovie', movie);
      } else {
        const success = await store.dispatch('getMovieDetails', id);
        if (!success) {
          next('/404');
        }
      }
      next();
    },
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'error',
    component: () => import(/* webpackChunkName: "error" */  '@/views/Error.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
