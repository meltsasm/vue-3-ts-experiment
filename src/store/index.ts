import { createStore } from 'vuex'
import { Movie, MoviedbResponse, RootState } from '@/models/models'
import { API_KEY, API_URL } from '@/config/config'

export default createStore({
  state: {
    popularMovies: [],
    selectedMovies: [],
    genreFilter: [],
    movie: null
  } as RootState,
  mutations: {
    setPopularMovies (state, payload) {
      state.popularMovies = payload
    },
    addSelectedMovies (state, payload: Movie) {
      state.selectedMovies.push(payload)
    },
    removeSelectedMovie (state, index: number) {
      state.selectedMovies.splice(index, 1)
    },
    setMovie (state, payload: Movie) {
      state.movie = payload
    },
    setGenreFilter (state, payload: number[]) {
      state.genreFilter = payload
    }
  },
  actions: {
    async loadPopularMovies ({ commit }): Promise<boolean> {
      const response: MoviedbResponse =
        await fetch(`${API_URL}/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}`)
      const result = await response.json()
      if (result) {
        this.commit('setPopularMovies', result.results)
        return true
      }
      return false
    },

    async getMovieDetails ({ commit }, movieId: string): Promise<boolean> {
      const response = await fetch(`https://api.themoviedb.org/3/movie/${movieId}?&api_key=${API_KEY}`)
      const result: Movie = await response.json()
      if (result.id) {
        this.commit('setMovie', result)
        return true
      }
      return false
    },

    addFavorites ({ commit }, movie: Movie) {
      if (!this.state.selectedMovies.some((m) => m.id === movie.id)) {
        this.commit('addSelectedMovies', movie)
      }
    },
    removeFromFavorites ({ commit }, movie: Movie) {
      const movieIndex = this.state.selectedMovies.findIndex((m) => m.id === movie.id)
      if (movieIndex !== -1) {
        this.commit('removeSelectedMovie', movieIndex)
      }
    },
    setMovie ({ commit }, movie: Movie) {
      this.commit('setMovie', movie)
    },
    updateGenreFilter ({ commit }, selectedGenres: string[]) {
      this.commit('setGenreFilter', selectedGenres)
    }
  },
  getters: {
    getPopularMovies (state): Movie[] {
      if (state.genreFilter.length) {
        const movies: Movie[] = state.popularMovies
        return movies.filter((movie: Movie) => movie.genre_ids
          .some((movieId: number) => state.genreFilter.includes(movieId)))
      }
      return state.popularMovies
    },
    getSelectedMovies (state): Movie[] {
      return state.selectedMovies
    },
    getActiveMovie (state): Movie | null {
      return state.movie
    }
  }
})
